import React from "react";
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";

const AboutUs = () => {
  return (
    <div>
      <Navs />

      <MDBRow className="g-0" style={{ marginTop: "100px" }}>
        <MDBCol md="12" lg="6" className="mb-4">
          <MDBCardBody>
            <div className="text-center mb-2">
              <span className="h1 fw-bold mb-0">
                <span style={{ color: "#fbbc04", fontWeight: "bolder" }}>
                  ABOUT
                </span>{" "}
                US
              </span>
            </div>

            <h6
              className="fw-normal my-4 pb-3"
              style={{
                letterSpacing: "1px",
                fontWeight: "bolder",
                color: "black",
                marginLeft: "30px",
                textAlign: "justify",
              }}
            >
              Cryptofibank is a group of friends established by three people
              (Lucas (British), Yoshiko (Japanese) and Kuzey (Turkish)) in order
              to earn passive income for themselves and their close circles. We
              provide our earnings through Forex exchange and Cryptocurrencies.
              Definitely not a pyramid scheme! As in every trade, there is a
              loss in our trade, but we do not reflect this loss to you. We have
              created percentage earnings plans according to certain investment
              amounts. Since we are not subject to the tax system of any
              country, we give percentages at rates that even banks do not.We
              offer a 24/7 withdrawal guarantee. There are absolutely no
              withdrawal limits or deductions. Only withdrawals are processed
              after 24 hours. The reason is that withdrawals from the global
              stock market do not occur immediately. Our mission is to win while
              you win. Cryptofibank does not support any institution or
              organization and does not give you investment advice.
            </h6>
            <MDBBtn
              color="dark"
              size="lg"
              style={{
                backgroundColor: "#fbbc04",
                color: "white",
                borderRadius: "6px",
                border: "none",
                maxWidth: "400px",
                marginLeft: "0",
              }}
            >
              Become a Member
            </MDBBtn>
          </MDBCardBody>
        </MDBCol>
        <MDBCol md="12" lg="6" className="mb-4">
          {/* <MDBCard> */}
          <MDBCardImage
            src="./Assets/11.png"
            alt="login form"
            style={{ height: "80%", width: "100%" }}
          />
          {/* </MDBCard> */}
        </MDBCol>
      </MDBRow>

      <section>
  <div className="text-center">
    <div className="section-title">
      <h2
        style={{
          fontWeight: "bold",
          color: "black",
          textDecoration: "underline",
          textDecorationColor: "#fbbc04",
        }}
      >
        SERVICES WE OFFER
      </h2>
      <h3 style={{ color: "black" }}>
        Explore the Wealth of Services at{" "}
        <span style={{ color: "#fbbc04" }}> CryptoFiBank </span>
      </h3>
    </div>
    <div className="row justify-content-center align-items-center">
    <div className="col-lg-3 col-md-6 mb-4 text-center">
  <div
    style={{
      backgroundColor: "white",
      width: "220px",
      height: "220px",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: "50%",
      border: "4px dotted #fbbc04",
      display: "flex",
      margin:'auto'
    }}
  >
    <div
      style={{
        backgroundColor: "#fbbc04",
        borderRadius: "50%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: "200px",
        height: "200px",
      }}
    >
      <h5
        style={{
          color: "white",
          margin: "0",
          fontWeight: "bolder",
        }}
      >
        Forex Trading
      </h5>
    </div>
  </div>
</div>


      {/* Repeat the above structure for the other columns */}
      
      <div className="col-lg-3 col-md-6 mb-4">
        <div
          style={{
            backgroundColor: "white",
            width: "220px",
            height: "220px",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "50%",
            border: "4px dotted #fbbc04",
            display: "flex",
            margin:'auto'
          }}
        >
          <div
            style={{
              backgroundColor: "#fbbc04",
              borderRadius: "50%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "200px",
              height: "200px",
            }}
          >
            <h5
              style={{
                color: "white",
                margin: "0",
                fontWeight: "bolder",
              }}
            >
              Cryptocurrencies
            </h5>
          </div>
        </div>
      </div>

      {/* Repeat the above structure for the other columns */}
      
      <div className="col-lg-3 col-md-6 mb-4">
        <div
          style={{
            backgroundColor: "white",
            width: "220px",
            height: "220px",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "50%",
            border: "4px dotted #fbbc04",
            display: "flex",
            margin:'auto'
          }}
        >
          <div
            style={{
              backgroundColor: "#fbbc04",
              borderRadius: "50%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "200px",
              height: "200px",
            }}
          >
            <h5
              style={{
                color: "white",
                margin: "0",
                fontWeight: "bolder",
              }}
            >
              Slot Games
            </h5>
          </div>
        </div>
      </div>

      {/* Repeat the above structure for the other columns */}
      
      <div className="col-lg-3 col-md-6 mb-4">
        <div
          style={{
            backgroundColor: "white",
            width: "220px",
            height: "220px",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "50%",
            border: "4px dotted #fbbc04",
            display: "flex",
            margin:'auto'
          }}
        >
          <div
            style={{
              backgroundColor: "#fbbc04",
              borderRadius: "50%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "200px",
              height: "200px",
            }}
          >
            <h5
              style={{
                color: "white",
                margin: "0",
                fontWeight: "bolder",
              }}
            >
              Lotteries
            </h5>
          </div>
        </div>
      </div>

      {/* Repeat the above structure for the other columns */}
      
    </div>
  </div>
</section>


      <Subscribe />
      <Footer />
    </div>
  );
};

export default AboutUs;
