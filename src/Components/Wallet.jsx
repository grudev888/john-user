import {React,useState} from "react";
import {
  MDBRow,
  MDBCol,
  MDBCardBody,
  MDBInput,
  MDBBtn,
  MDBContainer,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axios from "axios";

const Subscribe = () => {
  const [email, setemail] = useState("");
  const [Submitting, setSubmitting] = useState(false);

  const handleSubscribe = async () => {
    setSubmitting(true);

    try {
      const response = await axios.post(
        `${process.env.REACT_APP_URI}/Subscribe?email=${email}`,
        {
          headers: {
            "Content-Type": "application/json",
            "api-key": process.env.REACT_APP_API_KEY,
          },
        }
      );
        setemail('');
      setSubmitting(false);
    } catch (error) {
      console.error("Error:", error.message);
      setSubmitting(false);
    }
  };

  return (
    <section
      className="text-center mt-5"
      style={{ marginBottom: "40px", backgroundColor: "#fff" }}
    >
      <MDBContainer>
        <MDBRow className="justify-content-center">
          <MDBCol md="8" lg="8">
            <div
              className="testimonial-card"
              style={{ boxShadow: "0 0px 8px rgba(0, 0, 0, 0.5)" }}
            >
              <MDBCardBody>
                <h3
                  className="mb-4"
                  style={{ color: "#fbbc04", fontWeight: "bolder" }}
                >
                  SUBSCRIBE TO NEWS
                </h3>
                <h6
                  className="mb-4"
                  style={{ color: "black", fontWeight: "bolder" }}
                >
                  Get the Latest Insights: Subscribe to Our Newsletter
                </h6>
                <div
                  className="mb-4"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <MDBInput
                    wrapperClass="mb-4"
                    label="Enter Your Email Address"
                    id="formControlLg"
                    size="lg"
                    required
                    name="email"
                    value={email}
                    onChange={(e)=>{setemail(e.target.value)}}
                    style={{ width: "100%", maxWidth: "400px" }}
                  />
                  <MDBBtn
                    color="dark"
                    size="lg"
                    style={{
                      backgroundColor: "#fbbc04",
                      color: "white",
                      borderRadius: "30px",
                      border: "none",
                      width: "60%",
                      maxWidth: "400px",
                    }}
                    onClick={()=>{handleSubscribe()}}
                  >
                    {Submitting?(<MDBSpinner/>):(<span>Subscribe</span>)}
                    
                  </MDBBtn>
                </div>
              </MDBCardBody>
            </div>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </section>
  );
};

export default Subscribe;
