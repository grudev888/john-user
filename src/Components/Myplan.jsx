import { React, useState, useEffect } from "react";
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import CryptoJS from "crypto-js";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axios from "axios";
import Cookies from "js-cookie";

const Profile = () => {
  const [submitting, setSubmitting] = useState(false);
  const [username, setusername] = useState("");
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [address, setaddress] = useState("");
  const [refnumber, setrefnumber] = useState("");

  useEffect(() => {
    GetProfile();
  }, []);

  async function GetProfile() {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_URI}/GetProfile?email=${Cookies.get("email")}`,
        {
          method: "GET",
          headers: {
            "api-key": process.env.REACT_APP_API_KEY,
          },
        }
      );

      if (!response.ok) {
        throw new Error("Request failed.");
      }

      const data = await response.json();
      setusername(data.data[0].username);
      setname(data.data[0].name);
      setemail(data.data[0].email);
      setaddress(data.data[0].address);
      setrefnumber(data.data[0].refnumber);
    } catch (error) {
      console.error("Error:", error);
    }
  }

  const handleupdateProfile = async (e) => {
    e.preventDefault();
    setSubmitting(true);

    const form = e.target;
    const formData = new FormData(form);

    try {
      const response = await axios.post(
        `${process.env.REACT_APP_URI}/UpdateProfile`,
        formData,
        {
          headers: {
            "Content-Type": "application/json",
            "api-key": process.env.REACT_APP_API_KEY,
          },
        }
      );

      setSubmitting(false);
      form.reset();
      alert("Profile Updated Successfully");
    } catch (error) {
      console.error("Error:", error.message);
      setSubmitting(false);
    }
  };

  return (
    <div>
      <Navs />
      <MDBContainer>
        <MDBRow
          className="g-0"
          style={{
            marginTop: "100px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <MDBCol md="12" lg="8" className="mb-4">
            {/* <MDBCardBody className="d-flex flex-column"> */}
              <div className="d-flex flex-row mt-2 d-flex align-items-center justify-content-center">
                <h2
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    textDecoration: "underline",
                    textDecorationColor: "#fbbc04",
                  }}
                >
                  MY PROFILE
                </h2>
              </div>

              <MDBRow>
                <MDBCol md="12" lg="4" className="mb-4">
                  <img
                    src={`./assets/4.jpeg`}
                    alt="User"
                    style={{
                      borderRadius: "50%",
                      width: "160px",
                      height: "160px",
                      border: "8px solid #fbbc04",
                    }}
                  />
                </MDBCol>
                <MDBCol
                  md="12"
                  lg="3"
                  className="mb-4"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center", // Align items to the left
                    justifyContent: "center",
                  }}
                >
                  <div>
                    <h3
                      style={{
                        marginBottom: "10px",
                        fontWeight: "bolder",
                        color: "black",
                      }}
                    >
                      {name}
                    </h3>
                    <h4 style={{ marginBottom: "10px", fontWeight: "bolder" }}>
                      {refnumber}
                    </h4>
                    {/* Additional content can be added here */}
                  </div>
                </MDBCol>
              </MDBRow>

              <form
                encType="multipart/form-data"
                id="inventoryform"
                className="course-search-form"
                onSubmit={handleupdateProfile}
              >
                <div className="d-flex align-items-center">
                  <MDBInput
                    wrapperClass="mb-4"
                    label={"Username"}
                    id={"formControlLg"}
                    size={"lg"}
                    required
                    name="username"
                    value={username}
                    onChange={(e) => {
                      setusername(e.target.value);
                    }}
                  />
                </div>
                <div className="d-flex align-items-center">
                  <MDBInput
                    wrapperClass="mb-4"
                    label={"Your Name"}
                    id={"formControlLg"}
                    size={"lg"}
                    required
                    name="name"
                    value={name}
                    onChange={(e) => {
                      setname(e.target.value);
                    }}
                  />
                </div>
                <div className="d-flex align-items-center">
                  <MDBInput
                    wrapperClass="mb-4"
                    // type="email"
                    label={"Email"}
                    id={"formControlLg"}
                    size={"lg"}
                    required
                    name="email"
                    readOnly
                    value={email}
                    onChange={(e) => {
                      setemail(e.target.value);
                    }}
                  />
                </div>
                <div className="d-flex align-items-center">
                  <MDBInput
                    wrapperClass="mb-4"
                    label={"Just Enter The Tether ERC-20 Address"}
                    id={"formControlLg"}
                    size={"lg"}
                    required
                    name="address"
                    value={address}
                    onChange={(e) => {
                      setaddress(e.target.value);
                    }}
                  />
                </div>
                <div className="d-flex align-items-center">
                  <MDBInput
                    wrapperClass="mb-4"
                    label={"Enter Reference Number"}
                    id={"formControlLg"}
                    size={"lg"}
                    required
                    name="refnumber"
                    value={refnumber}
                    onChange={(e) => {
                      setrefnumber(e.target.value);
                    }}
                  />
                </div>
                <MDBBtn
                  className="mb-4 px-5"
                  color="dark"
                  size="lg"
                  style={{
                    backgroundColor: "#fbbc04",
                    color: "white",
                    borderRadius: "30px",
                    border: "none",
                    marginTop: "10px",
                  }}
                >
                  {submitting ? <MDBSpinner /> : <span>Save Now</span>}
                </MDBBtn>
              </form>
            {/* </MDBCardBody> */}
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <Subscribe />
      <Footer />
    </div>
  );
};

export default Profile;
