import React from 'react';
import { MDBFooter, MDBContainer, MDBRow, MDBCol, MDBIcon } from 'mdb-react-ui-kit';

export default function Footer() {
  return (
    <MDBFooter bgColor='light' className='text-center text-lg-start text-muted' style={{backgroundColor:'#f9ebbe'}}>
      {/* <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
        <div className='me-5 d-none d-lg-block'>
          <span>Get connected with us on social networks:</span>
        </div>

        <div>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="facebook-f" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="twitter" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="google" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="instagram" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="linkedin" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="github" />
          </a>
        </div>
      </section> */}

      <section className='' style={{backgroundColor:'#f9ebbe',padding:'30px',color:'black'}}>
        <MDBContainer className='text-center text-md-start mt-5'>
          <MDBRow className='mt-3'>
            <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4' style={{color:'black'}}>
              <img
              src="./assets/untitled-1-2-s8t.png"
              width="40"
              height="40"
              className="me-3"
            />
                {/* <MDBIcon icon="gem" className="me-3" /> */}
                CRYPTOFIBANK
              </h6>
              <p style={{color:'black'}}>
              CryptoFiBank, a trusted name in the world of financial opportunity, provides a secure platform for investing in forex, cryptocurrencies, slot games, and lotteries.
              </p>
            </MDBCol>

            <MDBCol md="2" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4' style={{color:'black'}}>Quick Links</h6>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                Terms of Service
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                Privacy Policy
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                Plans
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                Games
                </a>
              </p>
            </MDBCol>

            <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'style={{color:'black'}} >Company</h6>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                About us
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                  Contact Us
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                  Career
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset' style={{color:'black'}}>
                Lottery
                </a>
              </p>
            </MDBCol>

            <MDBCol md="4" lg="3" xl="3" className='mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4' style={{color:'black'}}>Connect With Us</h6>
              <div>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="facebook-f" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="twitter" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="whatsapp" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="instagram" />
          </a>
        </div>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-4' style={{backgroundColor:'#fbbc04',color:'white',fontWeight:'bolder'}}>
        Copyright:© 2024 All rights reserved
      </div>
    </MDBFooter>
  );
}