import React, { useEffect, useState } from "react";
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axios from "axios";
import Cookies from "js-cookie";
import Web3 from "web3";
import {
  MDBModal,
  MDBModalDialog,
  MDBModalContent,
  MDBModalHeader,
  MDBModalTitle,
  MDBModalBody,
  MDBModalFooter,
  MDBBadge,
  MDBSwitch,
} from "mdb-react-ui-kit";

const Plan = () => {
  const [submitting, setSubmitting] = useState(false);
  const [basicModal, setBasicModal] = useState(false);
  const [web3, setWeb3] = useState(null);
  const [Amount, setamount] = useState(0);
  const [plannumber, setplannumber] = useState(0);
  const [investmentperiod, setinvestmentperiod] = useState(0);
  const [gainratio, setgainratio] = useState(0);

  useEffect(() => {
    connectMetaMask();
  }, []);

  const connectMetaMask = async () => {
    try {
      if (window.ethereum) {
        const newWeb3 = new Web3(window.ethereum);
        await window.ethereum.enable();
        setWeb3(newWeb3);
        console.log("Connected to MetaMask");
      } else {
        console.error("MetaMask not found");
      }
    } catch (error) {
      console.error("Error connecting to MetaMask:", error);
    }
  };

  const makeTransaction = async (e) => {
    e.preventDefault(); // Prevent the default form submission

    try {
      // Check if MetaMask is installed and connected
      if (!window.ethereum || !window.ethereum.isConnected()) {
        console.error("MetaMask not connected");
        return;
      }
  
      // Request permission to access the user's MetaMask account
      const accounts = await window.ethereum.enable();
      if (!accounts || accounts.length === 0) {
        console.error("No MetaMask account connected");
        return;
      }
  
      const fromAddress = accounts[0];
      const amount = web3.utils.toWei((Amount * 500000000000000).toString(), "ether");
  
      // Build the transaction
      const transactionObject = {
        from: fromAddress,
        to: process.env.REACT_APP_METAMASKTOADDRESS,
        value: amount,
      };
  
      // Send the transaction
      const transactionHash = await web3.eth.sendTransaction(transactionObject);
      console.log("Transaction sent. Transaction hash:", transactionHash);
  
      // Wait for the transaction receipt
      const receipt = await web3.eth.getTransactionReceipt(transactionHash);
  
      // Check the status in the receipt
      if (receipt.status === "0x1") {
        console.log("Transaction successful");
        // Show success alert
        alert("Transaction successful");
  
        // Post transaction details to your server
        try {
          const response = await axios.post(
            `${process.env.REACT_APP_URI}/Plan?amount=${Amount}&email=${Cookies.get('email')}&plannumber=${plannumber}&investmentperiod=${investmentperiod}&gainratio=${gainratio}`,
            {},
            {
              headers: {
                "Content-Type": "application/json",
                "api-key": process.env.REACT_APP_API_KEY,
              },
            }
          );
          console.log("Server response:", response.data);
        } catch (error) {
          console.error("Error posting transaction details to the server:", error.message);
        }
      } else {
        console.error("Transaction failed");
        // Show error alert
        alert("Transaction failed");
      }
    } catch (error) {
      console.error("Error making transaction:", error);
      // Show error alert
      alert("Error making transaction");
     
    }
  };
  
  return (
    <div>
      <Navs />
      <section>
        <div>
          <img
            src="./assets/16.png"
            alt="Your Image"
            style={{ width: "100vw", height: "100%", marginTop: "84px" }}
          />
        </div>
      </section>

      <section style={{ marginTop: "80px" }}>
        <MDBRow
          className="flex justify-content-center align-item-center"
          style={{ margin: "20px" }}
        >
          <MDBCol md="4" lg="4" className="position-relative">
            <img src="./Assets/17.png" alt="" className="img-fluid" />

            {/* Button at the bottom center */}
            <MDBBtn
              className="position-absolute bottom-0 start-50 translate-middle-x"
              // size="lg"
              style={{
                backgroundColor: "#000",
                color: "white",
                borderRadius: "30px",
                border: "none",
                fontWeight: "bold",
                marginBottom: "27px",
                width: "30%",
                fontSize: "60%",
                height: "7%",
              }}
              onClick={() => {
                setplannumber(1);
                setinvestmentperiod(7);
                setgainratio(3);
                setBasicModal(true);
              }}
            >
              Buy Now
            </MDBBtn>
          </MDBCol>

          <MDBCol md="4" lg="4" className="position-relative">
            <img src="./Assets/18.png" alt="" className="img-fluid" />

            {/* Button at the bottom center */}
            <MDBBtn
              className="position-absolute bottom-0 start-50 translate-middle-x"
              // size="lg"
              style={{
                backgroundColor: "#000",
                color: "white",
                borderRadius: "30px",
                border: "none",
                fontWeight: "bold",
                marginBottom: "27px",
                width: "30%",
                fontSize: "60%",
                height: "7%",
              }}
              onClick={() => {
                setplannumber(2);
                setinvestmentperiod(7);
                setgainratio(14);
                setBasicModal(true);
              }}
            >
              Buy Now
            </MDBBtn>
          </MDBCol>

          <MDBCol md="4" lg="4" className="position-relative">
            <img src="./Assets/19.png" alt="" className="img-fluid" />

            {/* Button at the bottom center */}
            <MDBBtn
              className="position-absolute bottom-0 start-50 translate-middle-x"
              // size="lg"
              style={{
                backgroundColor: "#000",
                color: "white",
                borderRadius: "30px",
                border: "none",
                fontWeight: "bold",
                marginBottom: "27px",
                width: "30%",
                fontSize: "60%",
                height: "7%",
              }}
              onClick={() => {
                setplannumber(3);
                setinvestmentperiod(7);
                setgainratio(25);
                setBasicModal(true);
              }}
            >
              Buy Now
            </MDBBtn>
          </MDBCol>

          <MDBCol md="4" lg="4" className="position-relative">
            <img src="./Assets/20.png" alt="" className="img-fluid" />

            {/* Button at the bottom center */}
            <MDBBtn
              className="position-absolute bottom-0 start-50 translate-middle-x"
              // size="lg"
              style={{
                backgroundColor: "#000",
                color: "white",
                borderRadius: "30px",
                border: "none",
                fontWeight: "bold",
                marginBottom: "27px",
                width: "30%",
                fontSize: "60%",
                height: "7%",
              }}
              onClick={() => {
                setplannumber(4);
                setinvestmentperiod(30);
                setgainratio(70);
                setBasicModal(true);
              }}
            >
              Buy Now
            </MDBBtn>
          </MDBCol>

          <MDBCol md="4" lg="4" className="position-relative">
            <img src="./Assets/21.png" alt="" className="img-fluid" />

            {/* Button at the bottom center */}
            <MDBBtn
              className="position-absolute bottom-0 start-50 translate-middle-x"
              // size="lg"
              style={{
                backgroundColor: "#000",
                color: "white",
                borderRadius: "30px",
                border: "none",
                fontWeight: "bold",
                marginBottom: "27px",
                width: "30%",
                fontSize: "60%",
                height: "7%",
              }}
              onClick={() => {
                setplannumber(5);
                setinvestmentperiod(30);
                setgainratio(100);
                setBasicModal(true);
              }}
            >
              Buy Now
            </MDBBtn>
          </MDBCol>

          <MDBCol md="4" lg="4" className="position-relative">
            <img src="./Assets/22.png" alt="" className="img-fluid" />

            {/* Button at the bottom center */}
            <MDBBtn
              className="position-absolute bottom-0 start-50 translate-middle-x"
              // size="lg"
              style={{
                backgroundColor: "#000",
                color: "white",
                borderRadius: "30px",
                border: "none",
                fontWeight: "bold",
                marginBottom: "27px",
                width: "30%",
                fontSize: "60%",
                height: "7%",
              }}
              onClick={() => {
                setplannumber(6);
                setinvestmentperiod(30);
                setgainratio(150);
                setBasicModal(true);
              }}
            >
              Buy Now
            </MDBBtn>
          </MDBCol>
        </MDBRow>
      </section>
      <Subscribe />
      <Footer />

      <MDBModal show={basicModal} setShow={setBasicModal} tabIndex="-1">
        <MDBModalDialog style={{ borderRadius: 0 }}>
          <MDBModalContent id="card">
            <MDBModalHeader>
              <MDBModalTitle>Plan</MDBModalTitle>
              <MDBBtn
                className="btn-close"
                color="none"
                onClick={() => {
                  setBasicModal(false);
                }}
              ></MDBBtn>
            </MDBModalHeader>
            <form
              encType="multipart/form-data"
              id="inventoryform"
              className="course-search-form"
              onSubmit={makeTransaction}
            >
              <MDBModalBody>
                <img
                  src="./assets/untitled-1-2-s8t.png"
                  width="150"
                  height="150"
                  className="brandlogo d-inline-block align-top"
                  alt="CryptoFiBank"
                  style={{ marginTop: "-30px", marginBottom: "30px" }}
                />
                <div className="d-flex align-items-center">
                  <MDBInput
                    wrapperClass="mb-4"
                    label={"Amount"}
                    id={"formControlLg"}
                    size={"lg"}
                    required
                    name="username"
                    value={Amount}
                    onChange={(e) => {
                      setamount(e.target.value);
                    }}
                  />
                </div>
              </MDBModalBody>

              <MDBModalFooter>
                <MDBBtn
                  className="mb-4 px-5"
                  color="dark"
                  size="lg"
                  style={{
                    backgroundColor: "#fbbc04",
                    color: "white",
                    borderRadius: "30px",
                    border: "none",
                    marginTop: "10px",
                  }}
                  // onClick={()=>{makeTransaction();}}
                >
                  {submitting ? <MDBSpinner /> : <span>Submit</span>}
                </MDBBtn>
              </MDBModalFooter>
            </form>
          </MDBModalContent>
        </MDBModalDialog>
      </MDBModal>
    </div>
  );
};

export default Plan;
