import React, { useState, useEffect } from "react";
import {
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsContent,
  MDBTabsPane,
} from "mdb-react-ui-kit";
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import Web3 from 'web3';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
  MDBDatePicker,
} from "mdb-react-ui-kit";
import axios from "axios";
import Cookies from "js-cookie";

const Lottery = () => {
  const [activeTab, setActiveTab] = useState("Inquire By Ticket Number");

  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };

  const [selectAll, setSelectAll] = useState(false);
  const [checkedCount, setCheckedCount] = useState(0);

  useEffect(() => {
    // Update the Total Quantity when the checkboxes change
    const checkboxes = document.querySelectorAll('input[type="checkbox"]');
    const checkedCheckboxes = Array.from(checkboxes).filter(
      (checkbox) => checkbox.checked
    );
    setCheckedCount(checkedCheckboxes.length);
  }, [selectAll]);

  const handleSelectAll = (e) => {
    const checked = e.target.checked;
    setSelectAll(checked);

    // Get all checkboxes and set their checked state based on the "Select All" checkbox
    const checkboxes = document.querySelectorAll('input[type="checkbox"]');
    checkboxes.forEach((checkbox) => {
      checkbox.checked = checked;
    });
  };

  const handleCheckboxChange = () => {
    // Update the Total Quantity when individual checkboxes change
    const checkboxes = document.querySelectorAll('input[type="checkbox"]');
    const checkedCheckboxes = Array.from(checkboxes).filter(
      (checkbox) => checkbox.checked
    );
    setCheckedCount(checkedCheckboxes.length);
  };

  const [web3, setWeb3] = useState(null);

  useEffect(() => {
    connectMetaMask();
  }, []); 

  const connectMetaMask = async () => {
    try {
      if (window.ethereum) {
        const newWeb3 = new Web3(window.ethereum);
        await window.ethereum.enable();
        setWeb3(newWeb3);
        console.log("Connected to MetaMask");
      } else {
        console.error("MetaMask not found");
      }
    } catch (error) {
      console.error("Error connecting to MetaMask:", error);
    }
  };

  const makeTransaction = async () => {
    try {
      if (!web3) {
        console.error("MetaMask not connected");
        return;
      }
  
      // Get the connected MetaMask account address
      const accounts = await web3.eth.getAccounts();
      if (!accounts || accounts.length === 0) {
        console.error("No MetaMask account connected");
        return;
      }
  
      const fromAddress = accounts[0];
      const amount = web3.utils.toWei((checkedCount*500000000000000).toString(), 'ether'); // Amount in Wei
  
      // Build the transaction
      const transactionObject = {
        from: fromAddress,
        to: process.env.REACT_APP_METAMASKTOADDRESS,
        value: amount,
      };
  
      // Send the transaction
      const transactionHash = await web3.eth.sendTransaction(transactionObject);
      console.log("Transaction sent. Transaction hash:", transactionHash);
  
      // Wait for the transaction receipt
      const receipt = await web3.eth.getTransactionReceipt(transactionHash);
  
      // Check the status in the receipt
      if (receipt.status === '0x1') {
        console.log("Transaction successful");
        // Show success alert
        alert("Transaction successful");
        try {
          const response = await axios.post(
            `${process.env.REACT_APP_URI}/Lottery?quantity=${checkedCount}&email=${Cookies.get('email')}`,
            {
              headers: {
                "Content-Type": "application/json",
                "api-key": process.env.REACT_APP_API_KEY,
              },
            }
          );
        } catch (error) {
          console.error("Error:", error.message);
        }
    
      } else {
        console.error("Transaction failed");
        // Show error alert
        alert("Transaction failed");
        
      }
    } catch (error) {
      console.error("Error making transaction:", error);
      // Show error alert
      alert("Error making transaction");
      
    }
  };
  
  

  return (
    <div>
      <Navs />
      <header
        style={{
          paddingLeft: 0,
          marginTop: "86px",
          backgroundColor: "#fbbc04",
          paddingTop: "15px",
          paddingBottom: "15px",
        }}
      >
        <div
          className="text-center bg-image"
          style={{
            backgroundImage: "url('./assets/mask-group-nT2.png')",
            //   height: '100vh', // Default height for larger screens
            backgroundSize: "cover",
            backgroundPosition: "center center",
          }}
        >
          <div
            className="mask"
            style={{ backgroundColor: "rgba(0, 0, 0, 0.6)" }}
          >
            <div className="d-flex justify-content-center align-items-center h-100">
              <div className="text-white">
                <h2
                  className="mb-3 display-4 fw-bold text-uppercase p-5 "
                  style={{ marginTop: "30px" }}
                >
                  Take a Chance: Explore{" "}
                  <span style={{ color: "#fbbc04" }}> CryptoFiBank's </span>{" "}
                  Lotteries
                </h2>
                <p className="lead mb-3 p-5 " style={{ marginTop: "-100px" }}>
                  Welcome to CryptoFiBank's Monthly Lottery Draw, where your
                  dreams of winning big come to life on the
                  <span style={{ color: "#fbbc04" }}>
                    {" "}
                    5th of every month .
                  </span>{" "}
                  With 5,500 distinct tickets to choose from, each priced at
                  just $1, you have the power to pick your lucky number and test
                  your luck. The prize pot is set at an enticing $5,000, and our
                  website administrator will determine the winner, whose
                  username will be prominently displayed on the page once the
                  draw takes place. Don't miss your chance to become the next
                  lucky winner in our thrilling lottery event.
                </p>
              </div>
            </div>
          </div>
        </div>

        <style>
          {`
          @media (max-width: 767px) {
            .bg-image {
              height: 50vh; // Set height to 50% of the viewport height for screens up to 767px wide
            }

            .display-4 {
              font-size: 1rem; // Adjust the font size for smaller screens
            }

            .lead {
              font-size: 0.5rem; // Adjust the font size for smaller screens
            }

            .p-5 {
              margin-left: -40px;
              margin-right: -40px;
            }
          }

          @media (min-width: 768px) {
            .bg-image {
                height: 100vh; // Set height to 50% of the viewport height for screens up to 767px wide
              }

            .p-5 {
              margin-left: 100px;
              margin-right: 100px;
            }
          }
        `}
        </style>
      </header>

      <section style={{ marginTop: "50px" }}>
        <div className="text-center">
          <div className="section-title">
            <h3
              style={{
                fontWeight: "bold",
                color: "black",
                textDecoration: "underline",
                textDecorationColor: "#fbbc04",
              }}
            >
              MONTHLY LOTTERY DETAILS
            </h3>
          </div>
          <div className="row justify-content-center align-items-center">
            <div
              className="col-lg-2 col-md-3 mb-4 text-center"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  width: "150px",
                  height: "150px",
                  display: "flex",
                  flexDirection: "column",
                  borderRadius: "7px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.2)",
                }}
              >
                <div
                  style={{
                    flex: "30%",
                    backgroundColor: "#fbbc04",
                    color: "white",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h6 style={{ color: "white", fontWeight: "bold" }}>
                    Ending Date
                  </h6>
                </div>
                <div
                  style={{
                    flex: "70%",
                    backgroundColor: "transparent",
                    color: "black",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h6
                    style={{
                      color: "black",
                      fontWeight: "bold",
                      fontSize: "26px",
                    }}
                  >
                    5th <span style={{ fontSize: "10px" }}>of every month</span>
                  </h6>
                </div>
              </div>
            </div>

            {/* Repeat the above structure for the other columns */}

            <div
              className="col-lg-2 col-md-3 mb-4"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  width: "150px",
                  height: "150px",
                  display: "flex",
                  flexDirection: "column",
                  borderRadius: "7px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.2)",
                }}
              >
                <div
                  style={{
                    flex: "30%",
                    backgroundColor: "#fbbc04",
                    color: "white",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h6 style={{ color: "white", fontWeight: "bold" }}>
                    Total Tickets
                  </h6>
                </div>
                <div
                  style={{
                    flex: "70%",
                    backgroundColor: "transparent",
                    color: "black",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h4 style={{ color: "black", fontWeight: "bold" }}>5500</h4>
                </div>
              </div>
            </div>

            {/* Repeat the above structure for the other columns */}

            <div
              className="col-lg-2 col-md-3 mb-4"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  width: "150px",
                  height: "150px",
                  display: "flex",
                  flexDirection: "column",
                  borderRadius: "7px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.2)",
                }}
              >
                <div
                  style={{
                    flex: "30%",
                    backgroundColor: "#fbbc04",
                    color: "white",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h6 style={{ color: "white", fontWeight: "bold" }}>
                    Grand Prize
                  </h6>
                </div>
                <div
                  style={{
                    flex: "70%",
                    backgroundColor: "transparent",
                    color: "black",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h4 style={{ color: "black", fontWeight: "bold" }}>$5000</h4>
                </div>
              </div>
            </div>

            {/* Repeat the above structure for the other columns */}

            <div
              className="col-lg-2 col-md-3 mb-4"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  width: "150px",
                  height: "150px",
                  display: "flex",
                  flexDirection: "column",
                  borderRadius: "7px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.2)",
                }}
              >
                <div
                  style={{
                    flex: "30%",
                    backgroundColor: "#fbbc04",
                    color: "white",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h6 style={{ color: "white", fontWeight: "bold" }}>
                    Ticket Price
                  </h6>
                </div>
                <div
                  style={{
                    flex: "70%",
                    backgroundColor: "transparent",
                    color: "black",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h4 style={{ color: "black", fontWeight: "bold" }}>$1</h4>
                </div>
              </div>
            </div>

            <div
              className="col-lg-2 col-md-3 mb-4"
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                style={{
                  width: "150px",
                  height: "150px",
                  display: "flex",
                  flexDirection: "column",
                  borderRadius: "7px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.2)",
                }}
              >
                <div
                  style={{
                    flex: "30%",
                    backgroundColor: "#fbbc04",
                    color: "white",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h6 style={{ color: "white", fontWeight: "bold" }}>
                    Tickets Sold
                  </h6>
                </div>
                <div
                  style={{
                    flex: "70%",
                    backgroundColor: "transparent",
                    color: "black",
                    textAlign: "center",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <h4 style={{ color: "black", fontWeight: "bold" }}>250</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section style={{ marginTop: "50px" }}>
        <div className="text-center">
          {/* <MDBContainer> */}
          <MDBRow className="justify-content-center">
            <MDBCol md="8" lg="8">
              <div
                style={{
                  width: "100%",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.1)",
                  display: "flex",
                  flexDirection: "column",
                  borderTopLeftRadius: "10px",
                  overflow: "hidden",
                  borderTopRightRadius: "10px",
                }}
              >
                <div
                  style={{
                    backgroundImage: 'url("./Assets/15.png")',
                    backgroundSize: "cover",
                    height: "13%",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <h5
                    style={{
                      color: "white",
                      fontWeight: "bold",
                      textAlign: "center",
                      marginTop: "10px",
                    }}
                  >
                    SELECT & BUY{" "}
                    <span style={{ color: "#fbbc04" }}>YOUR TICKET </span>
                  </h5>
                  <p
                    style={{
                      color: "white",
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    Buy with the price of just one dollar and win a grand prize
                    of five thousand dollars
                  </p>
                </div>

                <MDBRow
                  className="justify-content-center"
                  style={{ backgroundColor: "#fbbc04" }}
                >
                  <MDBCol
                    md="4"
                    lg="4"
                    style={{ margin: "0px", padding: "0px" }}
                  >
                    {/* Second div with #fbbc04 background and responsive checkbox list */}
                    <div
                      style={{
                        backgroundColor: "#fbbc04",
                        flex: 1,
                        display: "flex",
                        flexDirection: "column",
                        padding: "20px",
                      }}
                    >
                      <div
                        style={{
                          backgroundColor: "black",
                          color: "white",
                          padding: "10px",
                          borderTopLeftRadius: "10px",
                          overflow: "hidden",
                          borderTopRightRadius: "10px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <label
                          style={{ display: "flex", alignItems: "center" }}
                        >
                          <strong>Select All</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={(e) => handleSelectAll(e)}
                          />
                        </label>
                      </div>
                      <div
                        style={{
                          overflowY: "auto",
                          backgroundColor: "white",
                          color: "black",
                          height: "110vh",
                          padding: "10px",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                        }}
                      >
                        {/* Use the handleCheckboxChange function for each checkbox's onChange event */}
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874136</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874137</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874138</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874139</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874140</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874141</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874142</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874143</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874144</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874145</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874146</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874147</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874148</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874149</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874150</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874151</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874152</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874153</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874154</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874155</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                      </div>
                    </div>
                  </MDBCol>
                  <MDBCol
                    md="4"
                    lg="4"
                    style={{ margin: "0px", padding: "0px" }}
                  >
                    <div
                      style={{
                        backgroundColor: "#fbbc04",
                        flex: 1,
                        display: "flex",
                        flexDirection: "column",
                        padding: "20px",
                      }}
                    >
                      <div
                        style={{
                          backgroundColor: "black",
                          color: "white",
                          padding: "10px",
                          borderTopLeftRadius: "10px",
                          overflow: "hidden",
                          borderTopRightRadius: "10px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <label
                          style={{ display: "flex", alignItems: "center" }}
                        >
                          <strong>Select All</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={(e) => handleSelectAll(e)}
                          />
                        </label>
                      </div>
                      <div
                        style={{
                          overflowY: "auto",
                          backgroundColor: "white",
                          color: "black",
                          height: "110vh",
                          padding: "10px",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                        }}
                      >
                        {/* Use the handleCheckboxChange function for each checkbox's onChange event */}
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874136</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874137</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874138</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874139</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874140</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874141</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874142</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874143</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874144</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874145</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874146</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874147</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874148</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874149</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874150</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874151</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874152</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874153</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874154</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874155</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                      </div>
                    </div>
                  </MDBCol>
                  <MDBCol
                    md="4"
                    lg="4"
                    style={{ margin: "0px", padding: "0px" }}
                  >
                    <div
                      style={{
                        backgroundColor: "#fbbc04",
                        flex: 1,
                        display: "flex",
                        flexDirection: "column",
                        padding: "20px",
                      }}
                    >
                      <div
                        style={{
                          backgroundColor: "black",
                          color: "white",
                          padding: "10px",
                          borderTopLeftRadius: "10px",
                          overflow: "hidden",
                          borderTopRightRadius: "10px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <label
                          style={{ display: "flex", alignItems: "center" }}
                        >
                          <strong>Select All</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={(e) => handleSelectAll(e)}
                          />
                        </label>
                      </div>
                      <div
                        style={{
                          overflowY: "auto",
                          backgroundColor: "white",
                          color: "black",
                          height: "110vh",
                          padding: "10px",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                        }}
                      >
                        {/* Use the handleCheckboxChange function for each checkbox's onChange event */}
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874136</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874137</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874138</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874139</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874140</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874141</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874142</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874143</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874144</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874145</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874146</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874147</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874148</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874149</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874150</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874151</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874152</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874153</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874154</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                        <label
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "1px solid #a5a5a5",
                            padding: "10px",
                            borderRadius: "5px",
                          }}
                        >
                          <strong>025874155</strong>
                          <input
                            type="checkbox"
                            style={{
                              marginLeft: "60px",
                              marginRight: "5px",
                              transform: "scale(1.5)",
                            }}
                            onChange={handleCheckboxChange}
                          />
                        </label>
                      </div>
                    </div>
                  </MDBCol>
                </MDBRow>

                {/* Third div with #fbbc04 background and black text */}
                <div
                  style={{
                    backgroundColor: "#FBBC04",
                    backgroundSize: "cover",
                    height: "13%",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    paddingBottom: "40px",
                  }}
                >
                  <div style={{ display: "flex", width: "100%" }}>
                    <h3
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                        marginBottom: "0",
                        flexBasis: "33%",
                      }}
                    >
                      <span style={{ fontSize: "14px" }}> Total Price</span>{" "}
                      <br /> ${checkedCount.toString()}
                    </h3>
                    <h3
                      style={{
                        color: "black",
                        fontWeight: "bold",
                        textAlign: "center",
                        marginBottom: "0",
                        flexBasis: "33%",
                      }}
                    >
                      <span style={{ fontSize: "14px" }}> Total Quantity</span>{" "}
                      <br /> {checkedCount.toString()}
                    </h3>
                    <MDBBtn
                      className="mb-4"
                      color="dark"
                      // size="lg"
                      style={{
                        backgroundColor: "#000",
                        color: "white",
                        fontWeight: "bold",
                        borderRadius: "5px",
                        border: "none",
                      }}
                      onClick={()=>{makeTransaction()}}
                    >
                      Buy Now
                    </MDBBtn>
                  </div>
                </div>
              </div>
            </MDBCol>
          </MDBRow>
          {/* </MDBContainer> */}
        </div>
      </section>

      <section style={{ marginTop: "50px" }}>
        <div className="text-center">
          <div className="section-title">
            <h3
              style={{
                color: "black",
                fontWeight: "bold",
                textDecoration: "underline",
                textDecorationColor: "#fbbc04",
              }}
            >
              TICKET INQUIRY
            </h3>
          </div>
          <MDBRow className="justify-content-center">
            <MDBCol
              md="8"
              lg="8"
              style={{
                boxShadow: "0 0 10px rgba(0, 0, 0, 0.1)",
                paddingBottom: "50px",
              }}
            >
              <MDBTabs className="mb-3">
                <MDBTabsItem style={{ width: "50%" }}>
                  <MDBTabsLink
                    onClick={() => handleTabClick("Inquire By Ticket Number")}
                    active={activeTab === "Inquire By Ticket Number"}
                    style={{
                      backgroundColor:
                        activeTab === "Inquire By Ticket Number"
                          ? "#fbbc04"
                          : "white",
                      color:
                        activeTab === "Inquire By Ticket Number"
                          ? "white"
                          : "black",
                      fontWeight: "bold",
                    }}
                  >
                    By Ticket Number
                  </MDBTabsLink>
                </MDBTabsItem>
                <MDBTabsItem style={{ width: "50%" }}>
                  <MDBTabsLink
                    onClick={() => handleTabClick("Inquire By Serial Number")}
                    active={activeTab === "Inquire By Serial Number"}
                    style={{
                      backgroundColor:
                        activeTab === "Inquire By Serial Number"
                          ? "#fbbc04"
                          : "white",
                      color:
                        activeTab === "Inquire By Serial Number"
                          ? "white"
                          : "black",
                      fontWeight: "bold",
                    }}
                  >
                    By Serial Number
                  </MDBTabsLink>
                </MDBTabsItem>
              </MDBTabs>
              <MDBRow className="justify-content-center mt-4">
                <MDBCol md="8" lg="8">
                  {activeTab === "Inquire By Ticket Number" && (
                    <div>
                      <MDBRow className="justify-content-center mt-4">
                        <MDBCol md="6" lg="6">
                          <MDBInput
                            wrapperClass="mb-4"
                            label={"Choose the Date (dd/mm/yy)"}
                            id={"formControlLg"}
                            size={"lg"}
                            required
                            name="refnumber"
                          />
                          <MDBInput
                            wrapperClass="mb-4"
                            label={"Enter Your Ticket Number"}
                            id={"formControlLg"}
                            size={"lg"}
                            required
                            name="refnumber"
                          />
                          <MDBBtn
                            className="mb-4 px-5"
                            color="dark"
                            size="lg"
                            style={{
                              backgroundColor: "#fbbc04",
                              color: "white",
                              borderRadius: "7px",
                              border: "none",
                              fontWeight: "bold",
                            }}
                          >
                            {" "}
                            INQUIRE NOW
                          </MDBBtn>
                        </MDBCol>
                      </MDBRow>
                    </div>
                  )}

                  {activeTab === "Inquire By Serial Number" && (
                    <div>
                      <MDBRow className="justify-content-center mt-4">
                        <MDBCol md="6" lg="6">
                          <MDBInput
                            wrapperClass="mb-4"
                            label={"Choose the Date (dd/mm/yy)"}
                            id={"formControlLg"}
                            size={"lg"}
                            required
                            name="refnumber"
                          />
                          <MDBInput
                            wrapperClass="mb-4"
                            label={"Enter Your Serial Number"}
                            id={"formControlLg"}
                            size={"lg"}
                            required
                            name="refnumber"
                          />
                          <MDBBtn
                            className="mb-4 px-5"
                            color="dark"
                            size="lg"
                            style={{
                              backgroundColor: "#fbbc04",
                              color: "white",
                              borderRadius: "7px",
                              border: "none",
                              fontWeight: "bold",
                            }}
                          >
                            {" "}
                            INQUIRE NOW
                          </MDBBtn>
                        </MDBCol>
                      </MDBRow>
                    </div>
                  )}
                </MDBCol>
              </MDBRow>
            </MDBCol>
          </MDBRow>
        </div>
      </section>

      <section style={{ marginTop: "50px" }}>
        <div className="text-center">
          <div className="section-title">
            <h3
              style={{
                fontWeight: "bold",
                color: "black",
                textDecoration: "underline",
                textDecorationColor: "#fbbc04",
              }}
            >
              LOTTERY RESULTS
            </h3>
          </div>
          {/* <MDBContainer> */}
          <MDBRow className="justify-content-center">
            <MDBCol
              md="8"
              lg="8"
              style={{
                boxShadow: "0 0 10px rgba(0, 0, 0, 0.1)",
              }}
            >
              <MDBRow className="g-0">
                <MDBCol
                  md="6"
                  className="d-flex align-items-center justify-content-center"
                  style={{ backgroundColor: "black" }}
                >
                  <img
                    src="./assets/13.png"
                    alt=""
                    style={{
                      width: "100%",
                      height: "100%",
                      marginLeft: "-25px",
                    }}
                  />
                </MDBCol>

                <MDBCol
                  md="6"
                  className="d-flex align-items-center justify-content-center"
                  // style={{backgroundImage:`url('./Assets/14.png')`, width:'100%',height:'100%'}}
                >
                  <img
                    src="./assets/14.png"
                    alt=""
                    style={{ width: "100%", height: "100%" }}
                  />
                </MDBCol>
              </MDBRow>
            </MDBCol>
          </MDBRow>
          {/* </MDBContainer> */}
        </div>
      </section>
      <Subscribe />
      <Footer />
    </div>
  );
};

export default Lottery;
