import React from "react";
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";

const FAQs = () => {
  return (
    <div>
      <Navs />
      <section style={{overflowX:'hidden'}}>
        <div className="row" style={{ marginTop: "100px" }}>
          {/* <div className="section-title"> */}
          <h2
            style={{
              fontWeight: "bold",
              color: "black",
            }}
          >
            <span style={{ color: "#fbbc04" }}>Frequently</span> Asked Questions
          </h2>
          {/* </div> */}
          <MDBRow
            style={{
              marginTop: "50px",
              backgroundColor: "#fbbc04",
              paddingTop: "5px",
              paddingBottom: "5px",
            }}
          >
            <MDBCol
              md="12"
              lg="1"
              style={{ backgroundColor: "white" }}
            ></MDBCol>
            <MDBCol
              md="12"
              lg="5"
              style={{
                backgroundColor: "white",
                marginRight: "5px",
                padding: "30px",
              }}
            >
              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Who is Cryptofibank?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Cryptofibank does not belong to anyone, it is a structure formed
                as a result of three friends coming together.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                What country is Cryptofibank in?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Cryptofibank does not anonymously support any country, political
                party or organization.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Why is there a USDT investment option on Cryptofibank?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Since Cryptofibank trades on the stock market, transactions are
                traded in dollars and accepts USDT payments in order not to
                disturb you due to the exchange rate difference of a different
                currency.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Is Cryptofibank reliable?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                You will give Cryptofibank's reliable rating, even if we say
                reliable, the final decision is yours. You can test its
                reliability with small units.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Is Cryptofibank a pyramidscheme?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Cryptofibank is definitely not a pyramid scheme. He does not
                distribute the money he cannot earn.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                How does Cryptofibank make money?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Cryptofibank makes money by trading on the forex exchange and
                crypto exchange.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                What is the minimum investmentamount in Cryptofibank?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Minimum investment amount at Cryptofibank is 200$
              </h6>
            </MDBCol>
            <MDBCol
              md="12"
              lg="5"
              style={{ backgroundColor: "white", padding: "30px"}}
            >
              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Are there any transaction fees at Cryptofibank?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                There are no transaction fees or any fees at Cryptofibank.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Is there a withdrawal limit on Cryptofibank?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                There is no withdrawal limit at Cryptofibank, but when you
                purchase an investment plan, withdrawals can be made when the
                purchased plan expires.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Does the withdrawal take place immediately at Cryptofibank?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Withdrawals and investments at Cryptofibank take place after 24
                hours, depending on the transaction intensity.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Is it possible to invest in Cryptofibank everyday?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Since Cryptofibank makes its transactions over the stock market,
                investments made on Saturday and Sunday are loaded into the
                account on Monday morning. The reason is that the stock market
                is closed on Saturday and Sunday.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Cryptofibank profit rates are not low?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Cryptofibank does not deceive you by declaring profit rates that
                it cannot give. It determines the profit rate it can give and
                presents it to you. Cryptofibank does not always win in the
                stock market, we also have losses, but we have not reflected
                this loss to any of our customers until now.
              </h6>

              <h5
                style={{
                  fontWeight: "bold",
                  color: "black",
                  marginTop: "30px",
                }}
              >
                {" "}
                Will Cryptofibank go bankrupt or shutdown the system over night?{" "}
              </h5>
              <h6
                style={{
                  color: "black",
                  textAlign: "justify",
                }}
              >
                Cryptofibank is quite good in its field (in the stock market).
                With enough capital and knowledge, it won't hurt you. As a
                matter of fact, we will suffer the same loss as you lost, so we
                do not take any action that will cause harm. Trust us,
                Cryptofibank is not something that will close over night. We are
                a structure that does not believe that trust can be bought with
                money. For this reason, we are trying to earn and win with all
                our strength in order not to lose your trust.
              </h6>
            </MDBCol>
            <MDBCol
              md="12"
              lg="1"
              style={{ backgroundColor: "white" }}
            ></MDBCol>
          </MDBRow>
        </div>
      </section>
      <Subscribe />
      <Footer />
    </div>
  );
};

export default FAQs;
