import React, { useState, useEffect } from 'react';
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";


const Game = () => {

  return (
    <div>
      <Navs />
      <section style={{ position: "relative" }}>
        <div style={{ position: "relative", textAlign: "center" }}>
          <img
            src="./assets/12.png"
            alt="Your Image"
            style={{ width: "100vw", height: "100%", marginTop: "84px" }}
          />
          <MDBBtn
            className="mb-4 px-5"
            color="dark"
            size="lg"
            style={{
              backgroundColor: "#fbbc04",
              color: "white",
              borderRadius: "30px",
              border: "none",
              fontWeight: "bold",
              position: "absolute",
              top: "80%",
              left: "50%",
              transform: "translateX(-50%)",
              fontSize: "16px", // Default font size
              padding: "12px 24px", // Default padding
              ...(window.innerWidth <= 768 && {
                fontSize: "14px", // Decreased font size for mobile views
                padding: "8px 16px", // Decreased padding for mobile views
              }),
            }}
          >
            Play Now
          </MDBBtn>
        </div>
      </section>

      <Subscribe />
      <Footer />
    </div>
  );
};

export default Game;
