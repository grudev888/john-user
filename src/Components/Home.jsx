import React from "react";
import Navs from "./Navbar";
import { MDBBtn, MDBRow, MDBCol } from "mdb-react-ui-kit";
import Cookies from "js-cookie";
import Subscribe from "./Subscribe";
import {
  MDBCarousel,
  MDBCarouselItem,
  MDBCard,
  MDBIcon,
  MDBCardBody,
  MDBInputGroup,
  MDBInput,
} from "mdb-react-ui-kit";
import Footer from "./Footer";

const Home = () => {
  const circleContainerStyle = {
    width: "100px",
    height: "100px",
    borderRadius: "50%",
    backgroundColor: "#fbbc04",
    boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)", // Box shadow for a subtle effect
    overflow: "hidden",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  const circleContainerStyle2 = {
    width: "100px",
    height: "100px",
    borderRadius: "50%",
    border: "5px solid #fbbc04",
    backgroundColor: "transparent",
    overflow: "hidden",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  const imageStyle = {
    maxWidth: "70%",
    maxHeight: "70%",
    // borderRadius: '50%',
  };
  const imageStyle2 = {
    maxWidth: "150px",
    maxHeight: "150px",
    // borderRadius: '50%',
  };
  return (
    <div>
      <Navs />
      <header style={{ paddingLeft: 0 }}>
        <div
          className="text-center bg-image"
          style={{
            backgroundImage:
              "url('./assets/gold-growing-arrow-with-gold-coin-money-stacks-gold-bar-business-finance-savings-investment-concept-background-3d-illustration-1.png')",
            //   height: '100vh', // Default height for larger screens
            backgroundSize: "cover",
            backgroundPosition: "center center",
          }}
        >
          <div
            className="mask"
            style={{ backgroundColor: "rgba(0, 0, 0, 0.6)" }}
          >
            <div className="d-flex justify-content-center align-items-center h-100">
              <div className="text-white">
                <h2
                  className="mb-3 display-4 fw-bold text-uppercase p-5 "
                  style={{ marginTop: "30px" }}
                >
                  Explore the Future of Passive Income with{" "}
                  <span style={{ color: "#fbbc04" }}>CryptoFiBank</span>
                </h2>
                <p className="lead mb-3 p-5 " style={{ marginTop: "-100px" }}>
                  Welcome to CryptoFiBank, where we offer you the key to unlock
                  a future of passive income. Empower your financial journey and
                  redefine your wealth generation with CryptoFiBank as we
                  accompany you on this exciting path to financial freedom.
                </p>
                <MDBRow>
                  <MDBCol
                    md="4"
                    className="d-flex align-items-center justify-content-center"
                  ></MDBCol>
                  <MDBCol
                    md="4"
                    className="d-flex align-items-center justify-content-center"
                  >
                    <MDBRow style={{ marginTop: "-15px" }}>
                      <MDBCol
                        md="6"
                        className="d-flex align-items-center justify-content-center"
                      >
                        <MDBBtn
                          className="mb-4 px-5"
                          color="dark"
                          // size="lg"
                          style={{
                            backgroundColor: "#fbbc04",
                            color: "white",
                            borderRadius: "5px",
                            border: "none",
                            marginTop: "-50px",
                          }}
                        >
                          Sign Up
                        </MDBBtn>
                      </MDBCol>
                      <MDBCol
                        md="6"
                        className="d-flex align-items-center justify-content-center"
                      >
                        <img
                          class="fluent-video-clip-multiple-16-filled-Pya"
                          src="./assets/fluent-video-clip-multiple-16-filled.png"
                          style={{
                            height: "40px",
                            marginRight: "10px",
                            marginTop: "-20px",
                          }}
                        />
                        <div class="group-2-6d6" style={{ marginTop: "-20px" }}>
                          <a class="watch-a-video-2Wk">Watch A Video</a>
                          <div class="line-1-vs2"></div>
                        </div>
                      </MDBCol>
                    </MDBRow>
                  </MDBCol>
                  <MDBCol
                    md="4"
                    className="d-flex align-items-center justify-content-center"
                  ></MDBCol>
                </MDBRow>
                <MDBRow>
                  <MDBCol
                    md="3"
                    className="d-flex align-items-center justify-content-center"
                  ></MDBCol>
                  <MDBCol
                    md="6"
                    className="d-flex align-items-center justify-content-center"
                  >
                    <MDBRow style={{ marginTop: "-15px" }}>
                      <MDBCol md="6" className="">
                        <h2
                          style={{ marginTop: "10px", display: "inline-block" }}
                          className="display-4 fw-bold text-uppercase"
                        >
                          30K{" "}
                          <span
                            className="lead fw-bold text-uppercase"
                            style={{ display: "inline-block" }}
                          >
                            Total Users
                          </span>
                        </h2>
                      </MDBCol>

                      <MDBCol
                        md="6"
                        className="d-flex align-items-center justify-content-center"
                      >
                        <h2
                          style={{ marginTop: "10px", display: "inline-block" }}
                          className="display-4 fw-bold text-uppercase"
                        >
                          15K{" "}
                          <span
                            className="lead fw-bold text-uppercase"
                            style={{ display: "inline-block" }}
                          >
                            Active Users
                          </span>
                        </h2>
                      </MDBCol>
                    </MDBRow>
                  </MDBCol>
                  <MDBCol
                    md="3"
                    className="d-flex align-items-center justify-content-center"
                  ></MDBCol>
                </MDBRow>
              </div>
            </div>
          </div>
        </div>

        <style>
          {`
          @media (max-width: 767px) {
            .bg-image {
              height: 80vh; // Set height to 50% of the viewport height for screens up to 767px wide
            }

            .display-4 {
              font-size: 1rem; // Adjust the font size for smaller screens
            }

            .lead {
              font-size: 0.7rem; // Adjust the font size for smaller screens
            }

            .p-5 {
              margin-left: -40px;
              margin-right: -40px;
            }
          }

          @media (min-width: 768px) {
            .bg-image {
                height: 100vh; // Set height to 50% of the viewport height for screens up to 767px wide
              }

            .p-5 {
              margin-left: 100px;
              margin-right: 100px;
            }
          }
        `}
        </style>
      </header>

      {/* <section className="single-course spad pb-0"> */}
      {/* <div className="container"> */}
      <div className="course-meta-area" style={{ backgroundColor: "#fbbc04" }}>
        <div className="row">
          <div className="col-lg-10 offset-lg-1">
            <h3 style={{ color: "white" }}>Our Trusted Partners</h3>
            <div className="course-metas">
              <div className="course-meta">
                <div className="cm-info">
                  <span
                    style={{
                      color: "white",
                      fontSize: "30px",
                      fontWeight: "bolder",
                    }}
                  >
                    Libertex
                  </span>
                </div>
              </div>
              <div className="course-meta">
                <div className="cm-info">
                  <span
                    style={{
                      color: "white",
                      fontSize: "30px",
                      fontWeight: "bolder",
                    }}
                  >
                    Binance
                  </span>
                </div>
              </div>
              <div className="course-meta">
                <div className="cm-info">
                  <span
                    style={{
                      color: "white",
                      fontSize: "30px",
                      fontWeight: "bolder",
                    }}
                  >
                    BYBIT
                  </span>
                </div>
              </div>
              <div className="course-meta">
                <div className="cm-info">
                  <span
                    style={{
                      color: "white",
                      fontSize: "30px",
                      fontWeight: "bolder",
                    }}
                  >
                    KuCoin
                  </span>
                </div>
              </div>
              <div className="course-meta">
                <div className="cm-info">
                  <span
                    style={{
                      color: "white",
                      fontSize: "30px",
                      fontWeight: "bolder",
                    }}
                  >
                    MEXC Global
                  </span>
                </div>
              </div>
              <div className="course-meta">
                <div className="cm-info">
                  <img
                    src="./assets/okx-seeklogocom-1.png"
                    alt=""
                    style={{ width: "50px" }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* </div> */}
      </div>
      {/* </section> */}

      <section className="categories-section spad">
        <div className="container">
          <div className="section-title">
            <h2
              style={{
                fontWeight: "bold",
                color: "black",
                textDecoration: "underline",
                textDecorationColor: "#fbbc04",
              }}
            >
              WHY CHOOSE US?
            </h2>
            <h3 style={{ color: "black" }}>
              Unlock the Advantages of{" "}
              <span style={{ color: "#fbbc04" }}>CryptoFiBank</span>
            </h3>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle}>
                    <img
                      src="./assets/shield-checkmark-black-icon-1.png"
                      alt="Your Image"
                      style={imageStyle}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    Trust & Transparency
                  </h4>
                  <p style={{ color: "black" }}>
                    At CryptoFiBank, we prioritize honesty and openness in all
                    our dealings. We stand firm in our commitment to providing a
                    trustworthy investment platform, assuring you that we are
                    not a pyramid scheme.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle}>
                    <img
                      src="./assets/skills-icon-1.png"
                      alt="Your Image"
                      style={imageStyle}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    Expertise in Forex and Cryptocurrency
                  </h4>
                  <p style={{ color: "black" }}>
                    We leverage this expertise to offer you strategic investment
                    solutions that harness the potential of these markets,
                    delivering you solid returns.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle}>
                    <img
                      src="./assets/hand-money-income-dollar-icon-1.png"
                      alt="Your Image"
                      style={imageStyle}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    Competitive Returns
                  </h4>
                  <p style={{ color: "black" }}>
                    At CryptoFiBank, we aim to help you grow your wealth at
                    rates that often surpass what conventional financial
                    institutions can offer, making your investment truly
                    worthwhile.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle}>
                    <img
                      src="./assets/pay-money-icon-1.png"
                      alt="Your Image"
                      style={imageStyle}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    24/7 Withdrawal Guarantee
                  </h4>
                  <p style={{ color: "black" }}>
                    Your financial freedom is our priority. With our 24/7
                    withdrawal guarantee, you have the flexibility to access
                    your earnings at any time, without withdrawal deductions.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle}>
                    <img
                      src="./assets/map-marker-flag-icon-1.png"
                      alt="Your Image"
                      style={imageStyle}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    Independence & Objectivity
                  </h4>
                  <p style={{ color: "black" }}>
                    we remain completely independent and do not align with any
                    institution or organization. We prioritize your financial
                    success and tailor our strategies to your unique needs."
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle}>
                    <img
                      src="./assets/lock-icon-1.png"
                      alt="Your Image"
                      style={imageStyle}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    Loss Mitigation
                  </h4>
                  <p style={{ color: "black" }}>
                    Our approach shields your investments from losses and
                    instead offers percentage-based earnings plans. Your peace
                    of mind is our priority as we navigate the ups and downs of
                    financial markets on your behalf.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div>
          <div className="section-title">
            <h2
              style={{
                fontWeight: "bold",
                color: "black",
                textDecoration: "underline",
                textDecorationColor: "#fbbc04",
              }}
            >
              What We Offer?
            </h2>
            <h3 style={{ color: "black" }}>
              Take a Gamble, Win Big:{" "}
              <span style={{ color: "#fbbc04" }}> Slot Games and Lottery </span>
              Adventures!
            </h3>
          </div>
          <div>
            <img
              src="./assets/1.png"
              alt="Your Image"
              style={{ width: "100vw", height: "100%" }}
            />
            <img
              src="./assets/2.png"
              alt="Your Image"
              style={{ width: "100vw", height: "100%" }}
            />
          </div>
        </div>
      </section>

      <section>
        <div className="container">
          <div className="section-title" style={{ marginTop: "30px" }}>
            <h2
              style={{
                fontWeight: "bold",
                color: "black",
                textDecoration: "underline",
                textDecorationColor: "#fbbc04",
              }}
            >
              TESTIMONIALS
            </h2>
            <h3 style={{ color: "black" }}>
              <span style={{ color: "#fbbc04" }}> CryptoFiBank </span>
              in the Words of Our Clients
            </h3>
          </div>

          <div className="row">
            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle2}>
                    <img
                      src="./assets/4.jpeg"
                      alt="Your Image"
                      style={imageStyle2}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    Amelia Taylor
                  </h4>
                  <h5 style={{ color: "black", fontWeight: "bolder" }}>
                    Canada
                  </h5>
                  <p style={{ color: "black" }}>
                    CryptoFiBank's slot games are not just fun but also
                    profitable. I hit a jackpot recently, and the payout was
                    quick and hassle-free. Their variety of slots keeps me
                    entertained for hours.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle2}>
                    <img
                      src="./assets/5.jpeg"
                      alt="Your Image"
                      style={imageStyle2}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                    James Smith
                  </h4>
                  <h5 style={{ color: "black", fontWeight: "bolder" }}>
                    Turkey
                  </h5>
                  <p style={{ color: "black" }}>
                    The support from CryptoFiBank's team is top-notch. Whenever
                    I've had questions or needed assistance, they've been quick
                    to respond and help. It adds an extra layer of confidence to
                    my investment experience.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6">
              <div
                className="categorie-item"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                }}
              >
                <div
                  className="ci-thumb set-bg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <div style={circleContainerStyle2}>
                    <img
                      src="./assets/6.jpeg"
                      alt="Your Image"
                      style={imageStyle2}
                    />
                  </div>
                </div>
                <div className="ci-text" style={{ marginTop: "-40px" }}>
                  <h4 style={{ color: "black", fontWeight: "bolder" }}>
                  Sarah Taylor
                  </h4>
                  <h5 style={{ color: "black", fontWeight: "bolder" }}>
                  UK
                  </h5>
                  <p style={{ color: "black" }}>
                  I've been a part of CryptoFiBank for a while, and their lottery games are a real thrill. The anticipation and excitement of each draw keep me coming back. I've won a few times, and it's always a great feeling.
                  </p>
                  {/* <span>{courseCount} Realms</span> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Subscribe />
      <Footer />
    </div>
  );
};

export default Home;
