import { React, useState } from "react";
// import "../styles/sign-up-page.css";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axios from "axios";

const SignUp = () => {
  const [submitting, setSubmitting] = useState(false);
  const handleSubmit = async (e) => {
    e.preventDefault();
    setSubmitting(true);

    const form = e.target;
    const formData = new FormData(form);

    try {
      const response = await axios.post(
        `${process.env.REACT_APP_URI}/register`,
        formData,
        {
          headers: {
            "Content-Type": "application/json",
            "api-key": process.env.REACT_APP_API_KEY,
          },
        }
      );

      const responseData = response.data;
      setSubmitting(false);

      if (responseData.message === "already") {
        alert("EMAIL OR USERNAME ALREADY EXISTS");
      } else {
        form.reset();
        alert("Register Successfully");
      }
    } catch (error) {
      console.error("Error:", error.message);
      setSubmitting(false);
    }
  };
  return (
    <div>
      {/* <MDBContainer className="my-5"> */}
      {/* <MDBCard > */}
      <MDBRow
        className="g-0"
        style={{ border: "0px solid #000", borderRadius: "10px" }}
      >
        <MDBCol md="6" style={{ position: "relative", textAlign: "center" }}>
          <img
            className="untitled-1-2-NWc"
            src="./assets/untitled-1-2-s8t.png"
            style={{
              width: "100px",
              height: "auto",
              position: "absolute",
              top: "30%",
              left: "25%",
              transform: "translate(-50%, -50%)",
            }}
          />
          <div>
            <h1
              style={{
                color: "#ffffff",
                fontSize: "1.5rem",
                fontWeight: "bold",
                textAlign: "center",
                position: "absolute",
                top: "30%",
                left: "55%",
                transform: "translate(-50%, -50%)",
              }}
            >
              CryptoFiBank
            </h1>
            <h3
              style={{
                color: "#ffffff",
                fontSize: "1rem",
                fontWeight: "bold",
                textAlign: "center",
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
              }}
              
            >
              The Crypto SignUp Page welcomes you to join our best platform.
            </h3>
          </div>
          <MDBCardImage
            src="./Assets/rectangle-8-bg-gmE.png"
            alt="login form"
            style={{ height: "100%" }}
          />
          <style>
            {`
      @media (max-width: 768px) {
        h1 {
          font-size: 1.2rem; /* Adjust the font size for small screens */
        }
        h3 {
          font-size: 0.8rem; /* Adjust the font size for small screens */
        }
      }
    `}
          </style>
        </MDBCol>

        <MDBCol md="6">
          <MDBCardBody className="d-flex flex-column">
            <div className="d-flex flex-row mt-2 d-flex align-items-center justify-content-center">
              {/* <MDBIcon
                    fas
                    icon="cubes fa-3x me-3"
                    style={{ color: "#ff6219" }}
                  /> */}
              <span className="h1 fw-bold mb-0">Member Form</span>
            </div>

            <h5
              className="fw-normal my-4 pb-3"
              style={{
                letterSpacing: "1px",
                fontWeight: "bolder",
                color: "black",
              }}
            >
              To stay in touch with us, please sign up to access Cryptofibank
            </h5>
            <form
              encType="multipart/form-data"
              id="inventoryform"
              className="course-search-form"
              onSubmit={handleSubmit}
            >
              <div className="d-flex align-items-center">
                <img
                  src={"./Assets/fluent-person-20-filled-nLQ.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Username"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="username"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/icon-park-solid-edit-name.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Your Name"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="name"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/wpf-name.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Your Surname"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="surname"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/clarity-email-solid.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  // type="email"
                  label={"Email"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="email"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/mdi-password.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  type="password"
                  label={"Password"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="password"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/entypo-address.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Just Enter The Tether ERC-20 Address"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="address"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/fluent-book-number-16-filled.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Enter Reference Number"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="refnumber"
                />
              </div>
              <MDBCheckbox
                name="flexCheck"
                style={{ backgroundColor: "#fbbc04" }}
                value=""
                id="flexCheckDefault"
                required
                label="I have read and approved the membership and confidentiality."
              />
              <MDBBtn
                className="mb-4 px-5"
                color="dark"
                size="lg"
                style={{
                  backgroundColor: "#fbbc04",
                  color: "white",
                  borderRadius: "30px",
                  border: "none",
                  marginTop: "10px",
                }}
              >
                {submitting?(<MDBSpinner/>):(<span>Sign Up</span>)}
                
              </MDBBtn>
            </form>
            <p className="mb-5 pb-lg-2" style={{ color: "#000" }}>
              Already have account?{" "}
              <a href="/SignIn" style={{ color: "#fbbc04" }}>
                SignIn
              </a>
            </p>
          </MDBCardBody>
        </MDBCol>
      </MDBRow>
      {/* </MDBCard> */}
      {/* </MDBContainer> */}
    </div>
  );
};

export default SignUp;
