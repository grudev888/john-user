import { React, useState } from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axios from "axios";
import {
  MDBModal,
  MDBModalDialog,
  MDBModalContent,
  MDBModalHeader,
  MDBModalTitle,
  MDBModalBody,
  MDBModalFooter,
  MDBBadge,
  MDBSwitch,
} from "mdb-react-ui-kit";
import Cookies from "js-cookie";

const SignIn = () => {
  const [basicModal, setBasicModal] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [submittingmodal, setsubmittingmodal] = useState(false);
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const handleLogin = async (event) => {
    setSubmitting(true);
    event.preventDefault();
    const data = {
      username: username,
      password: password,
    };

    await fetch(`${process.env.REACT_APP_URI}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "api-key": process.env.REACT_APP_API_KEY,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Request failed.");
        }
        return response.json();
      })
      .then((data) => {
        if (data.message === "success") {
          Cookies.set("email", data.email, { expires: 2 });
          Cookies.set("login", true, { expires: 2 });
          window.location.href = process.env.REACT_APP_URL;
        } else {
          setSubmitting(false);
          alert("Incorrect Login");
          setTimeout(() => {
            // setValid(false);
          }, 2000);
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };


  const handleForgot = async (e) => {
    e.preventDefault();
    setsubmittingmodal(true);

    const form = e.target;
    const formData = new FormData(form);

    try {
      const response = await axios.post(
        `${process.env.REACT_APP_URI}/forgotpassword`,
        formData,
        {
          headers: {
            "Content-Type": "application/json",
            "api-key": process.env.REACT_APP_API_KEY,
          },
        }
      );

      const responseData = response.data;
      setsubmittingmodal(false);

      if (responseData.message === "already") {
        alert("EMAIL OR USERNAME NOT EXISTS");
      } else {
        form.reset();
        setsubmittingmodal(false);
        alert("Recover Password Successfully");
        setsubmittingmodal(false);
      }
    } catch (error) {
      console.error("Error:", error.message);
      setsubmittingmodal(false);
    }
  };


  return (
    <div>
      {/* <MDBContainer className="my-5"> */}
      {/* <MDBCard > */}
      <MDBRow
        className="g-0"
        style={{
          border: "0px solid #000",
          borderRadius: "10px",
          height: "100vh",
        }}
      >
        <MDBCol
          md="6"
          style={{ position: "relative", textAlign: "center" }}
          className="d-flex align-items-center justify-content-center"
        >
          <img
            className="untitled-1-2-NWc"
            src="./assets/untitled-1-2-s8t.png"
            style={{
              width: "100px",
              height: "auto",
              position: "absolute",
              top: "30%",
              left: "25%",
              transform: "translate(-50%, -50%)",
            }}
          />
          <div>
            <h1
              style={{
                color: "#ffffff",
                fontSize: "1.5rem",
                fontWeight: "bold",
                textAlign: "center",
                position: "absolute",
                top: "30%",
                left: "55%",
                transform: "translate(-50%, -50%)",
              }}
            >
              CryptoFiBank
            </h1>
            <h3
              style={{
                color: "#ffffff",
                fontSize: "1rem",
                fontWeight: "bold",
                textAlign: "center",
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
              }}
            >
              The Crypto SignUp Page welcomes you to join our best platform.
            </h3>
          </div>
          <MDBCardImage
            src="./Assets/rectangle-8-bg-gmE.png"
            alt="login form"
            style={{ height: "100%" }}
          />
          <style>
            {`
      @media (max-width: 768px) {
        h1 {
          font-size: 1.2rem; /* Adjust the font size for small screens */
        }
        h3 {
          font-size: 0.8rem; /* Adjust the font size for small screens */
        }
      }
    `}
          </style>
        </MDBCol>

        <MDBCol
          md="6"
          className="d-flex align-items-center justify-content-center"
        >
          <MDBCardBody className="d-flex flex-column">
            <div className="d-flex flex-row mt-2 d-flex align-items-center justify-content-center">
              {/* <MDBIcon
                    fas
                    icon="cubes fa-3x me-3"
                    style={{ color: "#ff6219" }}
                  /> */}
              <span className="h1 fw-bold mb-0">Sign In</span>
            </div>

            <h5
              className="fw-normal my-4 pb-3"
              style={{
                letterSpacing: "1px",
                fontWeight: "bolder",
                color: "black",
              }}
            >
              To stay in touch with us, please sign in to access Cryptofibank
            </h5>
            <form
              encType="multipart/form-data"
              id="inventoryform"
              className="course-search-form"
              onSubmit={handleLogin}
            >
              <div className="d-flex align-items-center">
                <img
                  src={"./Assets/fluent-person-20-filled-nLQ.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Username"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="username"
                  value={username}
                  onChange={(e) => {
                    setusername(e.target.value);
                  }}
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/mdi-password.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  type="password"
                  label={"Password"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="password"
                  value={password}
                  onChange={(e) => {
                    setpassword(e.target.value);
                  }}
                />
              </div>
              <MDBCheckbox
                name="flexCheck"
                style={{ backgroundColor: "#fbbc04" }}
                value=""
                id="flexCheckDefault"
                label="Remember me"
              />
              <MDBBtn
                className="mb-4 px-5"
                color="dark"
                size="lg"
                // type="submit"
                style={{
                  backgroundColor: "#fbbc04",
                  color: "white",
                  borderRadius: "30px",
                  border: "none",
                  marginTop: "10px",
                }}
              >
                {submitting ? <MDBSpinner /> : <span>Sign In</span>}
              </MDBBtn>
            </form>
            <span
              onClick={() => {
                setBasicModal(true);
              }}
              style={{ color: "#fbbc04", cursor: "pointer" }}
            >
              Forgot Password?
            </span>
            <p className="mb-5 pb-lg-2" style={{ color: "#000" }}>
              Don't have an account?{" "}
              <a href="/SignUp" style={{ color: "#fbbc04" }}>
                Sign Up
              </a>
            </p>
          </MDBCardBody>
        </MDBCol>
      </MDBRow>
      {/* </MDBCard> */}
      {/* </MDBContainer> */}

      <MDBModal show={basicModal} setShow={setBasicModal} tabIndex="-1">
        <MDBModalDialog style={{ borderRadius: 0 }}>
          <MDBModalContent id="card">
            <MDBModalHeader>
              <MDBModalTitle>Login</MDBModalTitle>
              <MDBBtn
                className="btn-close"
                color="none"
                onClick={() => {
                  setBasicModal(false);
                }}
              ></MDBBtn>
            </MDBModalHeader>
            <form
              encType="multipart/form-data"
              id="inventoryform"
              className="course-search-form"
              onSubmit={handleForgot}
            >
              <MDBModalBody>
                <img
                  src="./assets/untitled-1-2-s8t.png"
                  width="150"
                  height="150"
                  className="brandlogo d-inline-block align-top"
                  alt="CryptoFiBank"
                  style={{ marginTop: "-30px" }}
                />
              <div className="d-flex align-items-center">
                <img
                  src={"./Assets/fluent-person-20-filled-nLQ.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  label={"Username"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="username"
                  value={username}
                  onChange={(e) => {
                    setusername(e.target.value);
                  }}
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/clarity-email-solid.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  // type="email"
                  label={"Email"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="email"
                />
              </div>
              <div className="d-flex align-items-center">
                <img
                  src={"./assets/mdi-password.png"}
                  alt="Custom Icon"
                  className="mr-3"
                  style={{ width: "24px", height: "24px" }}
                />
                <MDBInput
                  wrapperClass="mb-4"
                  type="password"
                  label={"Password"}
                  id={"formControlLg"}
                  size={"lg"}
                  required
                  name="password"
                />
              </div>
              </MDBModalBody>

              <MDBModalFooter>
                <MDBBtn
                  className="mb-4 px-5"
                  color="dark"
                  size="lg"
                  style={{
                    backgroundColor: "#fbbc04",
                    color: "white",
                    borderRadius: "30px",
                    border: "none",
                    marginTop: "10px",
                  }}
                >
                  {submittingmodal ? <MDBSpinner /> : <span>Submit</span>}
                </MDBBtn>
              </MDBModalFooter>
            </form>
          </MDBModalContent>
        </MDBModalDialog>
      </MDBModal>
    </div>
  );
};

export default SignIn;
