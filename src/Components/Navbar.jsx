import React, { useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Modal from "react-bootstrap/Modal";
import { Row, Col, Form } from "react-bootstrap";
import {
  MDBNavbarToggler,
  MDBCollapse,
  MDBIcon,
  MDBSpinner,
} from "mdb-react-ui-kit";
import {
  MDBDropdown,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBDropdownItem,
} from "mdb-react-ui-kit";
import { MDBRow, MDBCol, MDBInput } from "mdbreact";
import Cookies from "js-cookie";

const Navs = () => {
  const [showNavNoToggler, setShowNavNoToggler] = useState(false);
  const navbarStyle = {
    backgroundColor: "#FFF",
    zIndex: "100",
  };
  const handleLogout = () => {
    Cookies.remove("email");
    Cookies.remove("login");
    window.location.reload();
  };
  return (
    <div>
      <Navbar style={navbarStyle} expand="lg" fixed="top">
        <Navbar.Brand href="/" className="mr-auto">
          <a href="/">
            <img
              src="./assets/untitled-1-2-s8t.png"
              width="60"
              height="60"
              className="brandlogo d-inline-block align-top"
              alt="Tech Spec"
              //   style={{ marginLeft: "30px", marginRight: "18px" }}
            />
          </a>
        </Navbar.Brand>
        <h4 style={{ color: "#000", fontWeight: "bolder" }}>CryptoFiBank</h4>
        <MDBNavbarToggler
          type="button"
          data-target="#navbarTogglerDemo01"
          aria-controls="navbarTogglerDemo01"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={() => setShowNavNoToggler(!showNavNoToggler)}
        >
          <MDBIcon icon="bars" fas style={{ color: "black" }} />
        </MDBNavbarToggler>
        <MDBCollapse
          navbar
          show={showNavNoToggler}
          className="justify-content-end"
        >
          <Nav className="ml-auto">
            <Nav.Link
              style={{ color: "#000", marginRight: "15px",marginTop:'18px' }}
              href="/"
              id="Home"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              Home
            </Nav.Link>
            <Nav.Link
              style={{ color: "#000", marginRight: "15px" ,marginTop:'18px'}}
              href="/AboutUs"
              id="managa-list"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              About Us
            </Nav.Link>
            <Nav.Link
              style={{ color: "#000", marginRight: "15px" ,marginTop:'18px'}}
              href="/Plan"
              id="managa-list"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              Plans
            </Nav.Link>
            <Nav.Link
              style={{ color: "#000", marginRight: "15px" ,marginTop:'18px'}}
              href="/Game"
              id="shop"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              Game
            </Nav.Link>
            <Nav.Link
              style={{ color: "#000", marginRight: "15px" ,marginTop:'18px'}}
              href="/Lottery"
              id="shop"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              Lottery
            </Nav.Link>
            <Nav.Link
              style={{ color: "#000", marginRight: "15px" ,marginTop:'18px'}}
              href="/FAQs"
              id="shop"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              FAQs
            </Nav.Link>
            <Nav.Link
              style={{ color: "#000", marginRight: "15px" ,marginTop:'18px'}}
              href="/ContactUs"
              id="shop"
              className="bold"
              onMouseOver={(e) => {
                e.target.style.color = "#fbbc04";
              }}
              onMouseOut={(e) => {
                e.target.style.color = "#000";
              }}
            >
              Contact Us
            </Nav.Link>
            {Cookies.get("login") ? (
              <div style={{ backgroundColor: "transparent" }}>
                <Nav.Link>
                  <MDBDropdown>
                    <MDBDropdownToggle
                      style={{
                        background: "transparent",
                        border: "0",
                        boxShadow: "none",
                      }}
                    >
                      <img
                        src={`./assets/4.jpeg`}
                        alt="User"
                        style={{ borderRadius: "50%", width: "50px",height:'50px',border:'5px solid #fbbc04' }}
                      />
                    </MDBDropdownToggle>
                    <MDBDropdownMenu>
                      <MDBDropdownItem link onClick={()=>{window.location.href = '/Profile'}}>Update Profile</MDBDropdownItem>
                      <MDBDropdownItem
                        link
                        onClick={() => {
                          handleLogout();
                        }}
                      >
                        Logout
                      </MDBDropdownItem>
                    </MDBDropdownMenu>
                  </MDBDropdown>
                </Nav.Link>
              </div>
            ) : (
              <Nav.Link
                style={{
                  color: "#000",
                  marginRight: "15px",
                  backgroundColor: "#fbbc04",
                  paddingLeft: "50px",
                  paddingRight: "50px",
                  borderRadius: "5px",
                  marginTop:'18px'
                }}
                href="/SignIn"
                id="shop"
                className="bold"
                onMouseOver={(e) => {
                  e.target.style.color = "#FFF";
                }}
                onMouseOut={(e) => {
                  e.target.style.color = "#000";
                }}
              >
                LogIn
              </Nav.Link>
            )}
          </Nav>
        </MDBCollapse>
      </Navbar>

      <style>
        {`
          @media (max-width: 767px) {
            Navbar {
              background-color: white !important;
            }
          }
        `}
      </style>
    </div>
  );
};

export default Navs;
