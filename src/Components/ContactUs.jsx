import React, { useState } from "react";
import Navs from "./Navbar";
import Subscribe from "./Subscribe";
import Footer from "./Footer";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBRipple,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBCheckbox,
  MDBSpinner,
} from "mdb-react-ui-kit";
import axios from "axios";
import {
  MDBModal,
  MDBModalDialog,
  MDBModalContent,
  MDBModalHeader,
  MDBModalTitle,
  MDBModalBody,
  MDBModalFooter,
  MDBBadge,
  MDBSwitch,
} from "mdb-react-ui-kit";
import Cookies from "js-cookie";
import emailjs from "@emailjs/browser";

const ContactUs = () => {
  const [submitting, setSubmitting] = useState(false);
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [message, setmessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmitting(true);

    emailjs
      .send(
        process.env.REACT_APP_EMAILJS_SERVICE,
        process.env.REACT_APP_EMAILJS_TEMPLATE,
        {
          from_name: name,
          to_name: process.env.REACT_APP_YOURNAME,
          from_email: email,
          to_email: process.env.REACT_APP_YOUREMAIL,
          message: message,
        },
        process.env.REACT_APP_EMAILJS_PUBLICKEY
      )
      .then(
        () => {
          setSubmitting(false);
          alert("Thank you. I will get back to you as soon as possible.");
          setname("");
          setemail("");
          setmessage("");
        },
        (error) => {
          setSubmitting(false);
          console.error(error);
          alert("Ahh, something went wrong. Please try again.");
        }
      );
  };

  return (
    <div>
      <Navs />
        {/* <MDBContainer style={{padding:'0px', marginTop:'100px'}}> */}
          <MDBRow className="g-0" style={{marginTop:'100px'}}>
            <MDBCol md="12" lg="6" className="mb-4">
              <MDBCard>
                <MDBCardImage
                  src="./Assets/10.png"
                  alt="login form"
                  style={{ height: "100%" }}
                />
              </MDBCard>
            </MDBCol>

            <MDBCol md="12" lg="6" className="mb-4">
              <MDBCardBody>
                <div className="text-center mb-2">
                  <span className="h1 fw-bold mb-0">
                    <span style={{ color: "#fbbc04", fontWeight: "bolder" }}>
                      CONTACT
                    </span>{" "}
                    US
                  </span>
                </div>

                <h5
                  className="fw-normal my-4 pb-3"
                  style={{
                    letterSpacing: "1px",
                    fontWeight: "bolder",
                    color: "black",
                  }}
                >
                  CryptoFiBank - Your Trusted Financial Partner. Reach out to us
                  today.
                </h5>

                <form
                  encType="multipart/form-data"
                  id="inventoryform"
                  className="course-search-form"
                  onSubmit={handleSubmit}
                >
                  <div className="mb-4">
                    <MDBInput
                      wrapperClass="mb-4"
                      label={"Write Your Name"}
                      id={"formControlLg"}
                      size={"lg"}
                      required
                      name="name"
                      value={name}
                      onChange={(e) => {
                        setname(e.target.value);
                      }}
                    />
                  </div>
                  <div className="mb-4">
                    <MDBInput
                      wrapperClass="mb-4"
                      label={"Write Your Email Address"}
                      id={"formControlLg"}
                      size={"lg"}
                      required
                      name="email"
                      value={email}
                      onChange={(e) => {
                        setemail(e.target.value);
                      }}
                    />
                  </div>
                  <div className="mb-4">
                    <textarea
                      className="form-control z-depth-1"
                      id="exampleFormControlTextarea6"
                      rows="7"
                      placeholder="Write Your Message"
                      name="message"
                      style={{ width: "100%", height: "150px", fontSize: "17px" }}
                      value={message}
                      onChange={(e) => {
                        setmessage(e.target.value);
                      }}
                    ></textarea>
                  </div>
                  <MDBBtn
                    className="mb-4 px-5"
                    color="dark"
                    size="lg"
                    style={{
                      backgroundColor: "#fbbc04",
                      color: "white",
                      borderRadius: "30px",
                      border: "none",
                      marginTop: "10px",
                    }}
                  >
                    {submitting ? <MDBSpinner /> : <span>Send Message</span>}
                  </MDBBtn>
                </form>
              </MDBCardBody>
            </MDBCol>
          </MDBRow>
        {/* </MDBContainer> */}

      <Subscribe />
      <Footer />
    </div>
  );
};

export default ContactUs;
