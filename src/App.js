import "./App.css";
import { Switch, Route } from "react-router-dom";
import SignUp from "./Components/SignUp";
import SignIn from "./Components/SignIn";
import Home from "./Components/Home";
import ContactUs from "./Components/ContactUs";
import Profile from "./Components/Profile";
import AboutUs from "./Components/AboutUs";
import FAQs from "./Components/FAQs";
import Game from "./Components/Game";
import Lottery from "./Components/Lottery";
import Plan from "./Components/Plan";


function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/SignIn" component={SignIn} />
        <Route exact path="/SignUp" component={SignUp} />
        <Route exact path="/ContactUs" component={ContactUs} />
        <Route exact path="/Profile" component={Profile} />
        <Route exact path="/AboutUs" component={AboutUs} />
        <Route exact path="/FAQs" component={FAQs} />
        <Route exact path="/Game" component={Game} />
        <Route exact path="/Lottery" component={Lottery} />
        <Route exact path="/Plan" component={Plan} />
      </Switch>
    </div>
  );
}

export default App;
